#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
from importlib import import_module

def reload():
    _path = os.path.join(os.getcwd(), 'commands')
    sys.path.append(_path)

    ls = os.listdir(path='commands/')
    mod_ls = [ x[:x.index('.')] for x in ls if x != '__init__.py' ]

    mod = {}
    for module in mod_ls:
        mod[module] = import_module(module)

    cmd = {}
    for key, value in mod.items():
        cmd[key] = getattr(value, key)

    return mod, cmd
