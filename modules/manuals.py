#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018 SavoyRoad

# This file is part of SavoyBot.
# 
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

hblock = '''
`you have been blocked from using this bot.`
'''

herror ='''```
Problem remembering the command? Don't forget you can always run
.s $cmd -h if you need help!
```'''

hdiscord = (
            "```"
            "So it's either that the command isn't working as it should, "
            "or the Discord API doesn't like you (reeeeeee proprietary). "
            "One of the two\n\n"
            "Make sure you're typing the command correctly! "
            "Type .s $cmd -h to view the help menu for said command"
            "```"
            )

hdberror ='''```
Looks like you haven't saved a setting! Type ".s save -h" to find out how!
```'''

hsave ='''```
Saves user data for use in commands.

usage:      .s save [OPTIONS] ...
options:
            [-l]    :   Saves your preferred unit of measurement and your
            location for use in pulling up the weather.
            Accepted units of measure include:
                m = Metric
                u = US
            e.g. ".s save -l m Liverpool, GB"
            [-f]    :   Saves your LastFM username
            e.g. ".s save -f Username"
```'''

hpasta ='''```
Prints out the selected copypasta, replacing keywords with those supplied by
the user.

usage:      .s pasta [OPTIONS] <TEXT>
options:
            [-l, --linux]   :   Prints the GNU/Linux copypasta, replacing
            TWO values for "GNU" and "Linux"
            [-a, --apple]   :   Prints the Apple copypasta, replacing
            ONE value for "Apple"
            [-c, --cummies] :   Prints the cummies copypasta, replacing
            ONE value for "Daddy"
            [-g, --good]    :   Prints the good shit copypasta, replacing TWO
            values for "good" and "shit"
            [-d, --dicks]    :   Prints the dicks  copypasta, replacing ONE
            value for "Dicks"
```'''

hweather ='''```
Displays the weather for the selected location.
Must save a preferred unit of measurement first, e.g. ".s save -l m Liverpool".
Type ".s save -h" for more details.

usage:      .s weather [location, user]
options:    [location]  :   Outputs the weather for the selected location
            e.g. .s weather Liverpool, GB
            [user]      :   Outputs the weather for the selected user if saved
            e.g. .s weather @User
            If your location is saved:
            e.g. .s weather
```'''

hsponge ='''```
Can either output a user-supplied sentence with alternating capitalization, or
a selected copypasta following the rules of the .s sponge command.

usage:      .s sponge [OPTIONS] <TEXT>
options:
            Please see .s pasta -h for more information on extra options.
```'''

hddg ='''```
Searches DuckDuckGo and prints the results on the page.

usage:      .s ddg [OPTIONS] <SEARCH TERM>
            .s ddg -w urban Savoy
            .s ddg -j -w yt Blank Space
options:
            [-w] site.com : Searches a specific site, either through a SITE
            SHORTCUT or its domain name
            [-j]          : Brings up the first search result and shows the
            resulting embed

List of site shortcuts:
            Amazon      :   a
            YouTube     :   yt
            Wikipedia   :   wiki
            UrbanDict   :   urban
            Dictionary  :   d
            Thesaurus   :   t
```'''

hsay ='''```
Prints user input through cowsay. Default is cow.

usage:      .s say [OPTIONS] <TEXT>
options:
            -t, -v, -m, -s, -w
            The fun is testing what flag does what :P
```'''

hfm ='''```
Shows the following stats from LastFM:
    Now Playing
    Last Scrobbled
    Top 10 Artists for the past week
Also works with a saved LastFM name through .s save -f SavoyRoad

usage:      .s fm [user]
            [user]      :   Outputs the weather for the selected user if saved
            e.g. .s fm @User
            e.g. .s fm LastFM_Username
            If your username is saved:
            e.g. .s fm
```'''

hclap ='''```
Places an emoji between each uppercased word.

usage:      .s clap <TEXT>
```'''
