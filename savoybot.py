#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

# Import libraries used in __main__
import os
import sys
import socket
import sqlite3
import discord
from discord.ext import commands
from importlib import import_module

# Import bot modules
h = os.path.expanduser('~')
sys.path.append('./modules/'.format(h))
import config
import options
import manuals
import sql_init
import reload

# Refreshes the command modules
mod, cmd = reload.reload()

# Setup of Bot
bot = commands.Bot(command_prefix='.')

# SQL connection
sql = sql_init.SQLInit()

# Logging
if socket.gethostname() == config.vps_host:
    import logging
    logger = logging.getLogger('discord')
    logger.setLevel(logging.DEBUG)
    handler = logging.FileHandler(filename='discord.log', encoding='utf-8',
            mode='w')
    handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s:%(message)s'))
    logger.addHandler(handler)


def block_list(user):
    '''Reloads the block list for commands'''
    sql.cur.execute("SELECT id, status from block where status = 1")
    blocks = sql.cur.fetchall()
    blocked_users = dict(blocks)
    if user in blocked_users:
        return True


@bot.event
async def on_ready():
    # Logs the bot into Discord
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')
    await bot.change_presence(game=discord.Game(name='type ".s man" for help'))


@bot.event
async def on_message(message):
    '''Replies with an emoji if mentioned'''
    if message.author == bot.user:
        return
    if block_list(message.author.id):
        await bot.say('{}, {}'.format(ctx.message.author.mention,
                                      manuals.hblock))
    if message.content.startswith(bot.user.mention):
        output = [
            value for key, value in options.emoji.items() if value in
            message.content
        ]
        if output:
            output = ' '.join(output)
            msg = '{}, {}'.format(message.author.mention, output)
            await bot.send_message(message.channel, msg)
        else:
            return
    await bot.process_commands(message)


@bot.command(pass_context=True)
async def s(ctx, msg, *message):
    '''Dynamically parses the message and runs the correct command'''
    if (
            block_list(ctx.message.author.id) and
            ctx.message.author.id not in config.root
    ):
        await bot.say('{}, {}'.format(ctx.message.author.mention,
                                      manuals.hblock))
    elif msg == 's':
        if ctx.message.author.id in config.root:
            msg = ' '.join(message)
            await bot.delete_message(ctx.message)
            await bot.say(msg)
        else:
            await bot.say(
                '{}, {}'.format(
                    ctx.message.author.mention,config.emoji['smirk'])
            )
    else:
        output = cmd[msg](ctx, message)
        try:
            await bot.say(embed=output)
        except AttributeError:
            await bot.say(output)
        except discord.errors.HTTPException:
            await bot.say(manuals.hdiscord)

bot.run(config.token)
