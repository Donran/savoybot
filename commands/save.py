#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

import manuals
import options
import sqlite3
import discord

import sql_init
sql = sql_init.SQLInit()

def save(ctx, message):
    '''Saves the chosen option into the bot DB.'''

    if message[0] in options.helps:
        return manuals.hsave
    if message[0] == options.saves['location']:
        embed = discord.Embed(color=0xf0c674)
        location = ' '.join(message[2:])
        unit = ''.join(message[1])
        user = ctx.message.author.name
        user_id = ctx.message.author.id
        values = (user_id, user, unit, location)
        vuser = (user_id,)
        try:
            sql.cur.execute("""
                    insert into weather
                    values (?, ?, ?, ?)
                    """, values)
            sql.conn.commit()
            embed.add_field(
                name='Setting Saved',
                value='{0}, your location has been set to {1}'.format(
                    ctx.message.author.mention, location)
            )
            return embed
        except sqlite3.IntegrityError:
            sql.cur.execute("""
                    delete from weather
                    where id = ?
                    """, vuser)
            sql.cur.execute("""
                    insert into weather
                    values (?, ?, ?, ?)
                    """, values)
            sql.conn.commit()
            embed.add_field(
                name='Setting Saved',
                value='{0}, your location has been changed to {1}'.format(
                    ctx.message.author.mention, location)
            )
            return embed

    elif message[0] == options.saves['lastfm']:
        embed = discord.Embed(color=0xf0c674)
        name = ' '.join(message[1:])
        user = ctx.message.author.name
        user_id = ctx.message.author.id
        values = (user_id, user, name)
        vuser = (user_id,)
        try:
            sql.cur.execute("""
                    insert into lastfm
                    values (?, ?, ?)
                    """, values)
            sql.conn.commit()
            embed.add_field(
                name='Setting Saved',
                value='{0}, your LastFM Username has been set to {1}'.format(
                    ctx.message.author.mention, name)
            )
            return embed
        except sqlite3.IntegrityError:
            sql.cur.execute("""
                    delete from lastfm
                    where id = ?
                    """, vuser)
            sql.cur.execute("""
                    insert into lastfm
                    values (?, ?, ?)
                    """, values)
            sql.conn.commit()
            embed.add_field(
                name='Setting Saved',
                value='{0}, your LastFM Username has been changed to {1}'.format(
                    ctx.message.author.mention, name)
            )
            return embed
