#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

import pyowm
import sqlite3
import config
import discord
import manuals
import options

import sql_init
sql = sql_init.SQLInit()

# OpenWeatherMap
owm = pyowm.OWM(config.wapi)

def weather(ctx, message):
    '''Outputs weather for user, other user, or chosen city.'''
    try:
        try:
            if message[0] in options.helps:
                return manuals.hweather
            elif ctx.message.mentions:
                user_id = (ctx.message.mentions[0].id,)
                sql.cur.execute(
                    "select unit, location from weather where id = ?",
                    user_id)
                results = sql.cur.fetchall()
                unit = options.units[results[0][0]]
                loc = results[0][1]
            else:
                unit = 'celsius'
                loc = ' '.join(message)

        except IndexError:
            if ctx.message.mentions:
                pass
            else:
                user_id = (ctx.message.author.id,)
                sql.cur.execute(
                    "select unit, location from weather where id = ?",
                    user_id)
                results = sql.cur.fetchall()
                unit = options.units[results[0][0]]
                loc = results[0][1]

        location = loc.split(',')

        try:
            city = location[0]
            country = location[1].lstrip()
            place = '{}, {}'.format(city, country)
        except IndexError:
            city = location[0]
            country = ''
            place = city

        obs = owm.weather_at_place(loc)
        w = obs.get_weather()
        code = w.get_weather_code()
        status1 = w.get_status()
        status2 = w.get_detailed_status()

        if code in range(200, 599):
            prep = w.get_rain()
            try:
                prep = prep['3h']
                prep_description = ' mm of rain'
            except KeyError:
                prep = 'Minimal precipitation'
                prep_description = ' '
        elif code in range(600, 699):
            prep = w.get_snow()
            try:
                prep = prep['3h']
                prep_description = ' mm of snow'
            except KeyError:
                prep = 'Minimal precipitation'
                prep_description = ' '
        else:
            prep = 'No precipitation'
            prep_description = ' '

        time = w.get_reference_time(timeformat='iso')
        clouds = w.get_clouds()
        wind = w.get_wind()
        humidity = w.get_humidity()
        temp = w.get_temperature(unit=unit)
        icon = w.get_weather_icon_name()
        icon_url = 'http://openweathermap.org/img/w/{}.png'.format(icon)

        main = (
            '{} - {}\n'
            '{}°{}'
            .format(status1, status2, temp['temp'], unit[0].upper())
        )
        details = (
            '{}% humidity\n'
            '{}% cloud cover\n'
            '{}{}\n'
            '{} km/h wind speed'
            .format(humidity, clouds, prep, prep_description, wind['speed'])
        )

        embed = discord.Embed(color=0x81a2be)
        embed.set_thumbnail(url=icon_url)
        embed.set_author(name=place)
        embed.add_field(name='Current Weather at\n{}'.format(time),
                        value=main,
                        inline=False
                        )
        embed.add_field(name='Details',
                        value=details,
                        inline=False
                        )
        return embed

    except IndexError:
        return manuals.herror
    except (UnboundLocalError, sqlite3.DatabaseError):
        if message[0] in manuals.helps:
            pass
        else:
            return manuals.hdberror
    except discord.errors.HTTPException:
        return manuals.hdiscord
