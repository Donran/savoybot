#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

import discord
import sqlite3
import sql_init
import config

sql = sql_init.SQLInit()

def block(ctx, message):
    '''Sets block status for given user'''
    if ctx.message.author.id not in config.root:
        return 'http://i.imgur.com/fAXM9SB.gif'
    status_dict = {
            1    :   'blocked',
            0    :   'unblocked'
    }

    if message[0] in ['-s', '--status']:
        embed = discord.Embed(color=0xde935f)
        sql.cur.execute(
            "SELECT name, status from block where status = 1"
        )
        results = sql.cur.fetchall()
        output = []
        for row in results:
            output.append(': '.join((row[0], status_dict[row[1]])))
        if output:
            joutput = '\n'.join(output)
            embed.add_field(name='Block List', value=output)
            return embed
        else:
            embed.add_field(name='Block List', value='Nobody blocked!')
            return embed

    else:
        embed = discord.Embed(color=0xcc6666)
        user_id = ctx.message.mentions[0].id
        user = ctx.message.mentions[0].name
        values = (user_id, user, 1)
        try:
            sql.cur.execute(
                "SELECT status from block where id=?", [user_id])
            status = sql.cur.fetchall()
            if status[0][0] == 1:
                values = (user_id, user, 0)
            sql.cur.execute("INSERT OR REPLACE INTO block values (?, ?, ?)",
                        values)
            sql.conn.commit()
        except (sqlite3.DatabaseError, IndexError):
            sql.cur.execute("INSERT OR REPLACE INTO block values (?, ?, ?)",
                        values
            )
            sql.conn.commit()
            status = 1

        embed.add_field(name='Update' ,
                        value='{} has been {}'.format(user,
                                                      status_dict[values[2]])
                        )
        return embed
