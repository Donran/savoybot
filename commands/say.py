#!/usr/bin/env python3

# Simple Discord bot
# Copyright (C) 2018 SavoyRoad

# This file is part of SavoyBot.
#
# SavoyBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SavoyBot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SavoyBot.  If not, see <http://www.gnu.org/licenses/>.

import manuals
import options
import text

def say(ctx, message):
    '''Outputs message through chosen collection of "cowsay" options.'''
    arg = message[0]

    if arg in options.helps:
        return manuals.hsay
    elif arg[0] != '-':
        msg = ' '.join(message)
        out = text.cow.replace('ARG1', msg)
        return out
    elif arg in options.says:
        msg = ' '.join(message[1:])
        out = options.says[arg].replace('ARG1', msg)
        return out
    else:
        return manuals.herror

